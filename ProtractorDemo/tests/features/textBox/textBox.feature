Feature: Text Box

    Scenario: User fill all the fields
        Given User goes to "https://demoqa.com/text-box"
        When User fill all the fields with values
            | Name | Email            | CurrentAddress | PermanentAddress |
            | John | john@example.com | USA            | USA              |
        And Clicks on submit button
        Then User should see correct informations
            | Name | Email            | CurrentAddress | PermanentAddress |
            | John | john@example.com | USA            | USA              |

    Scenario Outline: User fill only certain fields
        Given User goes to "https://demoqa.com/text-box"
        When User fill certain fields "<Field>" with value "<Value>"
        And Clicks on submit button
        Then User should see that field "<Field>" and values "<Value>"

        Examples:
            | Field            | Value |
            | Name             | John  |
            | CurrentAddress   | USA   |
            | PermanentAddress | USA   |

    Scenario Outline: User tries to submit invalid email format
        Given User goes to "https://demoqa.com/text-box"
        When User input invalid email format "<Value>"
        And Clicks on submit button
        Then Email value should not be displayed

        Examples:
            | Value          |
            | john@          |
            | john@gmail     |
            | john@gmail.    |