Feature: Radio buttons
    
    Scenario Outline: User does one of the possible clicks
        Given User goes to "https://demoqa.com/radio-button"
        When User clicks on "<RadioButton>" radio button
        Then User should see radio result message "<Message>"

        Examples:
            | RadioButton | Message    |
            | Yes         | Yes        |
            | Impressive  | Impressive |
