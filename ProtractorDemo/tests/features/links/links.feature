Feature: Links

    Scenario Outline: User clicks on link wich opens a new tab
        Given User goes to "https://demoqa.com/links"
        When User clicks on a link "<LinkName>"
        Then New tab url should be "https://demoqa.com/"

        Examples:
            | LinkName  |
            | Home      |
            | HomeHVThd |

    Scenario Outline: User clicks on one of the possible links
        Given User goes to "https://demoqa.com/links"
        When User clicks on a link "<LinkName>"
        Then User should see response message "<Message>"

        Examples:
            | LinkName     | Message                                                             |
            | Created      | Link has responded with staus 201 and status text Created           |
            | No Content   | Link has responded with staus 204 and status text No Content        |
            | Moved        | Link has responded with staus 301 and status text Moved Permanently |
            | Bad Request  | Link has responded with staus 400 and status text Bad Request       |
            | Unauthorized | Link has responded with staus 401 and status text Unauthorized      |
            | Forbidden    | Link has responded with staus 403 and status text Forbidden         |
            | Not Found    | Link has responded with staus 404 and status text Not Found         |
    
    Scenario: User clicks on broken link
        Given User goes to "https://demoqa.com/broken"
        When User clicks on a link "Click Here for Broken Link"
        Then New url should be "http://the-internet.herokuapp.com/status_codes/500"

    Scenario: User clicks on valid link
        Given User goes to "https://demoqa.com/broken"
        When User clicks on a link "Click Here for Valid Link"
        Then New url should be "https://demoqa.com/"

