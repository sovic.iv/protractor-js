Feature: Buttons

    Scenario Outline: User does one of the possible clicks
        Given User goes to "https://demoqa.com/buttons"
        When User does a "<TypeOfClick>"
        Then User should see message "<Message>" for "<TypeOfClick>"

        Examples:
            | TypeOfClick  | Message                       |
            | double click | You have done a double click  |
            | right click  | You have done a right click   |
            | click        | You have done a dynamic click |



