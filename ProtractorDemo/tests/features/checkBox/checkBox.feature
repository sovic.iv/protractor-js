Feature: Check Box

    Scenario Outline: User checks all options with Home checkbox
        Given User goes to "https://demoqa.com/checkbox"
        When User clicks on Home checkbox
        Then User should see result message "<Message>"

        Examples:
            | Message                                                                                                                                 |
            | home desktop notes commands documents workspace react angular veu office public private classified general downloads wordFile excelFile |
    
    Scenario Outline: User chooses checkbox that he wants
        Given User goes to "https://demoqa.com/checkbox"
        When User clicks on Expand all button
        And Chooses checkboxes that he wants "<Checkbox>"
        Then User should see result message "<Message>"

        Examples:
            | Checkbox  | Message                      |
            | Desktop   | desktop notes commands       |
            | Veu       | veu                          |
            | Downloads | downloads wordFile excelFile |