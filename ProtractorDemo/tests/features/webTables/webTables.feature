Feature: Web tables

    Scenario Outline: User edit some values in row
        Given User goes to "https://demoqa.com/webtables"
        When User clicks on edit icon
        And Inputs new value "<Value>" in field "<Field>"
        And Clicks on submit new row button
        Then New value should be shown "<Value>"

        Examples:
            | Value       | Field      |
            | Vegga       | Last Name  |
            | Cierra Nora | First Name |
            | 3000        | Salary     |

    Scenario Outline: User adds new row with values
        Given User goes to "https://demoqa.com/webtables"
        When User clicks on add new button
        And Fill in form with values
            | FirstName   | LastName   | Age   | Email   | Salary   | Department   |
            | <FirstName> | <LastName> | <Age> | <Email> | <Salary> | <Department> |
        And Clicks on submit new row button
        Then User should see wanted informations
            | FirstName   | LastName   | Age   | Email   | Salary   | Department   |
            | <FirstName> | <LastName> | <Age> | <Email> | <Salary> | <Department> |

        Examples:
            | FirstName | LastName | Age | Email            | Salary | Department |
            | John      | Square   | 38  | john@example.com | 2200   | HR         |

    Scenario Outline: User does search by value
        Given User goes to "https://demoqa.com/webtables"
        When User does search by "<Value>"
        Then User should see wanted informations
            | FirstName   | LastName   | Age   | Email   | Salary   | Department   |
            | <FirstName> | <LastName> | <Age> | <Email> | <Salary> | <Department> |

        Examples:
            | Value | FirstName | LastName | Age | Email             | Salary | Department |
            | Ca    | Alden     | Cantrell | 45  | alden@example.com | 12000  | Compliance |

