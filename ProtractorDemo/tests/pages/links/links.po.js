'use strict';

const { element } = require("protractor");

let link,
    responseMessage;

module.exports = {
    get: (string) => {
        return browser.get(string);
    },
    getLink: (value) => {
        link = element(by.partialLinkText(value))
        return link
    },
    getResponseMessage:()=>{
        responseMessage = element(by.id('linkWrapper')).element(by.id('linkResponse')).getText()
        return responseMessage
    }
}