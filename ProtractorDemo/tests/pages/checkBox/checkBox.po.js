'use strict';

const { element } = require("protractor");

let homeCheckBox,
    result,
    expandAllButton,
    checkBox;

module.exports = {
    init: () => {
        homeCheckBox = element(by.className('rct-title')),
            expandAllButton = element(by.className('rct-option rct-option-expand-all'))
    },
    get: (string) => {
        return browser.get(string);
    },
    getHomeCheckBox: () => {
        return homeCheckBox;
    },
    getResultTextValue: () => {
        result = element(by.id('result')).getText()
        return result
    },
    getExpandAllButton: () => {
        return expandAllButton
    },
    getCheckBox: (value) => {
        checkBox = element(by.cssContainingText('.rct-title', value))
        return checkBox
    }
}