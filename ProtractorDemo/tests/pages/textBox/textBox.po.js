'use strict';

let nameField,
    emailField,
    currentAddrField,
    permanentAddrField,
    nameValue,
    emailValue,
    currentAddrValue,
    permanentAddrValue,
    submitButton;

module.exports = {
    init: () => {
        nameField = element(by.id('userName'))
        emailField = element(by.id('userEmail'))
        currentAddrField = element(by.id('currentAddress'))
        permanentAddrField = element(by.id('permanentAddress'))
    },
    get: (string) => {
        return browser.get(string);
    },
    setNameField: (name) => {
        return nameField.sendKeys(name);
    },
    setEmailField: (email) => {
        return emailField.sendKeys(email);
    },
    setCurrentAddrField: (currAddr) => {
        return currentAddrField.sendKeys(currAddr);
    },
    setPermanentAddrField: (permAddr) => {
        return permanentAddrField.sendKeys(permAddr);
    },
    getNameValue: () => {
        nameValue = element(by.id('name'))
        return nameValue.getText()
    },
    getEmailValue: () => {
        emailValue = element(by.id('email'))
        return emailValue.getText()
    },
    getEmailValueField: () => {
        emailValue = element(by.id('email'))
        return emailValue
    },
    getCurrentAddrValue: () => {
        currentAddrValue = element(by.id('output')).element(by.id('currentAddress'))
        return currentAddrValue.getText()
    },
    getPermanentAddrValue: () => {
        permanentAddrValue = element(by.id('output')).element(by.id('permanentAddress'))
        return permanentAddrValue.getText()
    },
    getSubmitButton: () => {
        submitButton = element(by.id('submit'))
        return submitButton
    }
}