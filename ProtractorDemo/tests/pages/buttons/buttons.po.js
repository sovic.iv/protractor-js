'use strict';

const { element } = require("protractor");

let doubleClickButton,
    rightClickButton,
    clickButton,
    message;

module.exports = {
    init: () => {
        doubleClickButton = element(by.id('doubleClickBtn'))
        rightClickButton = element(by.id('rightClickBtn'))
        clickButton = element(by.buttonText('Click Me'))
    },
    get: (string) => {
        return browser.get(string);
    },
    getDoubleClickButton: () => {
        return doubleClickButton;
    },
    getRightClickButton: () => {
        return rightClickButton
    },
    getClickButton: () => {
        return clickButton
    },
    getDoubleClickMessage: () => {
        message = element(by.id('doubleClickMessage')).getText()
        return message
    },
    getRightClickMessage: () => {
        message = element(by.id('rightClickMessage')).getText()
        return message
    },
    getClickMessage: () => {
        message = element(by.id('dynamicClickMessage')).getText()
        return message
    }
}