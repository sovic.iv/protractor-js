'use strict';

const { element } = require("protractor");

let yesRadioButton,
    impressiveRadioButton,
    messageTitle,
    messageText;

module.exports = {
    init: () => {
        yesRadioButton = element(by.id('yesRadio'))
        impressiveRadioButton = element(by.id('impressiveRadio'))
    },
    get: (string) => {
        return browser.get(string);
    },
    getYesRadioButton: () => {
        return yesRadioButton;
    },
    getImpressiveRadioButton: () => {
        return impressiveRadioButton
    },
    getMessageTitle: () => {
        messageTitle = element(by.partialLinkText('You have selected '))
        return messageTitle
    },
    getMessageText: () => {
        messageText = element(by.className('text-success')).getText()
        return messageText
    }
}