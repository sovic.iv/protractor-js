'use strict';

const { element } = require("protractor");

let editFirstRow,
    lastNameField,
    firstNameField,
    salaryField,
    ageField,
    emailField,
    departmentField,
    submitButton,
    addButton,
    searchField;

module.exports = {
    init: () => {
        editFirstRow = element(by.id('edit-record-1'))
        addButton = element(by.id('addNewRecordButton'))
        searchField = element(by.id('searchBox'))
    },
    get: (string) => {
        return browser.get(string);
    },
    getEditIcon: () => {
        return editFirstRow
    },
    getLastNameField: () => {
        lastNameField = element(by.id('lastName'))
        return lastNameField
    },
    getSubmitButton: () => {
        submitButton = element(by.id('submit'))
        return submitButton
    },
    getFirstNameField: () => {
        firstNameField = element(by.id('firstName'))
        return firstNameField
    },
    getSalaryField: () => {
        salaryField = element(by.id('salary'))
        return salaryField
    },
    getAddButton: () => {
        return addButton
    },
    getAgeField: () => {
        ageField = element(by.id('age'))
        return ageField
    },
    getEmailField: () => {
        emailField = element(by.id('userEmail'))
        return emailField
    },
    getDepartmentField: () => {
        departmentField = element(by.id('department'))
        return departmentField
    },
    getFieldValue: (fieldName) => {
        return element(by.cssContainingText('.rt-td', fieldName)).getText()
    },
    getSearchField: () => {
        return searchField
    }
}