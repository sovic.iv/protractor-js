let specPage = require('../../pages/textBox/textBox.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout, Then, When } = require('cucumber');
const { browser } = require('protractor');
var EC = protractor.ExpectedConditions;
setDefaultTimeout(60 * 1200);

Before(function () {
    specPage.init();
})

Given('User goes to {string}', function (string) {
    specPage.get(string);
});

When('User fill all the fields with values', async function (dataTable) {
    var informations = dataTable.hashes()
    specPage.setNameField(informations[0].Name)
    specPage.setEmailField(informations[0].Email)
    specPage.setCurrentAddrField(informations[0].CurrentAddress)
    specPage.setPermanentAddrField(informations[0].PermanentAddress)
})

Then('Clicks on submit button', function () {
    var button = specPage.getSubmitButton()
    var isClickable = EC.elementToBeClickable(button)
    browser.wait(isClickable, 5000)
    button.click()
})

Then('User should see correct informations', async function (dataTable) {
    var informations = dataTable.hashes()
    await specPage.getNameValue().then(text => {
        expect(text).to.equal('Name:' + informations[0].Name)
    })
    await specPage.getEmailValue().then(text => {
        expect(text).to.equal('Email:' + informations[0].Email)
    })
    await specPage.getCurrentAddrValue().then(text => {
        expect(text).to.equal('Current Address :' + informations[0].CurrentAddress)
    })
    await specPage.getPermanentAddrValue().then(text => {
        expect(text).to.equal('Permananet Address :' + informations[0].PermanentAddress)
    })
})

When('User fill certain fields {string} with value {string}', function (field, value) {
    switch (field) {
        case "Name":
            specPage.setNameField(value)
            break;
        case "Email":
            specPage.setEmailField(value)
            break;
        case "PermanentAddress":
            specPage.setPermanentAddrField(value)
            break;
        case "CurrentAddress":
            specPage.setCurrentAddrField(value)
            break
        default:
            throw new Error('Field value is required');
    }
})

Then('User should see that field {string} and values {string}', async function (field, value) {
    switch (field) {
        case "Name":
            await specPage.getNameValue().then(text => {
                expect(text).to.equal('Name:' + value)
            })
            break;
        case "Email":
            await specPage.getEmailValue().then(text => {
                expect(text).to.equal('Email:' + value)
            })
            break;
        case "PermanentAddress":
            await specPage.getPermanentAddrValue().then(text => {
                expect(text).to.equal('Permananet Address :' + value)
            })
            break;
        case "CurrentAddress":
            await specPage.getCurrentAddrValue().then(text => {
                expect(text).to.equal('Current Address :' + value)
            })
            break;
        default:
            throw new Error('Field value is required');
    }
})

When('User input invalid email format {string}', function (value) {
    specPage.setEmailField(value)
})

Then('Email value should not be displayed', function () {
    let emailValue = specPage.getEmailValueField()
    expect(emailValue.isDisplayed()).to.eventually.equal(false)
})