let specPage = require('../../pages/links/links.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout, Then, When } = require('cucumber');
const { browser } = require('protractor');
setDefaultTimeout(60 * 1200);

When('User clicks on a link {string}', function (value) {
    specPage.getLink(value).click()
})

Then('User should see response message {string}', async function (value) {
    var response = await specPage.getResponseMessage()
    expect(response).to.equal(value)
})

Then('New tab url should be {string}', function (value) {
    browser.getAllWindowHandles().then(function (handles) {
        browser.switchTo().window(handles[1]).then(function () {
            expect(browser.getCurrentUrl().to.equal(value))
        });
    });
})

Then('New url should be {string}', async function (value) {
    var url = await browser.getCurrentUrl()
    expect(url).to.equal(value)
})