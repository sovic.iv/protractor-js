let specPage = require('../../pages/radioButton/radioButton.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout, Then, When } = require('cucumber');
const { browser, element } = require('protractor');
const { protractor } = require('protractor/built/ptor');
setDefaultTimeout(60 * 1200);

When('User clicks on {string} radio button', async function (value) {
    switch (value) {
        case "Yes":
            await browser.actions().click(element(by.id('yesRadio'))).perform()
            break;
        case "Impressive":
            await browser.actions().click(element(by.id('impressiveRadio'))).perform()
            break;
        default:
            throw new Error('Type of button must be defined');
    }
})

Then('User should see radio result message {string}', async function (value) {
    switch (value) {
        case "Yes":
            await specPage.getMessageText().then(text => {
                expect(text).to.equal(value)
            })
            break;
        case "Impressive":
            await specPage.getMessageText().then(text => {
                expect(text).to.equal(value)
            })
            break;
        default:
            throw new Error('Message text must be defined');
    }
})