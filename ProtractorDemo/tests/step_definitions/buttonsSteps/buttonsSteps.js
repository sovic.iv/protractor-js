let specPage = require('../../pages/buttons/buttons.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout, Then, When } = require('cucumber');
const { browser } = require('protractor');
var EC = protractor.ExpectedConditions;
setDefaultTimeout(60 * 1200);

Before(function () {
    specPage.init();
})

When('User does a {string}', async function (value) {
    switch (value) {
        case "double click":
            browser.actions().doubleClick(specPage.getDoubleClickButton()).perform()
            break;
        case "right click":
            await browser.actions().click(specPage.getRightClickButton(), protractor.Button.RIGHT).perform()
            break;
        case "click":
            await browser.actions().click(specPage.getClickButton()).perform()
            break;
        default:
            throw new Error('Type of click must be defined');
    }

})

Then('User should see message {string} for {string}', async function (expectedMessage, value) {
    switch (value) {
        case "double click":
            browser.sleep(5000)
            await specPage.getDoubleClickMessage().then(text => {
                expect(text).to.equal(expectedMessage)
            })
            break;
        case "right click":
            browser.sleep(10000)
            await specPage.getRightClickMessage().then(text => {
                expect(text).to.equal(expectedMessage)
            })
            break;
        case "click":
            browser.sleep(5000)
            await specPage.getClickMessage().then(text => {
                expect(text).to.equal(expectedMessage)
            })
            break;
        default:
            throw new Error('Type of click must be defined');
    }
})