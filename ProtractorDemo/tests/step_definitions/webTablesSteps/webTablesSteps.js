let specPage = require('../../pages/webTables/webTables.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var informations
var { setDefaultTimeout, Then, When, Before } = require('cucumber');
const { browser, element } = require('protractor');
setDefaultTimeout(60 * 1200);

Before(function () {
    specPage.init();
})

When('User clicks on edit icon', async function () {
    await browser.actions().click(specPage.getEditIcon()).perform()
})

Then('Inputs new value {string} in field {string}', async function (value, field) {
    switch (field) {
        case "First Name":
            await specPage.getFirstNameField().clear().sendKeys(value)
            break;
        case "Last Name":
            await specPage.getLastNameField().clear().sendKeys(value)
            break;
        case "Salary":
            await specPage.getSalaryField().clear().sendKeys(value)
            break;
        default:
            throw new Error('Name of field must be defined');
    }
})

Then('New value should be shown {string}', async function (value) {
    await element(by.cssContainingText('.rt-td', value)).getText().then(text => {
        expect(text).to.equal(value)
    })
})

Then('Clicks on submit new row button', async function () {
    await browser.actions().click(specPage.getSubmitButton()).perform()
})

When('User clicks on add new button', async function () {
    await browser.actions().click(specPage.getAddButton()).perform()
})

Then('Fill in form with values', function (dataTable) {
    informations = dataTable.hashes()
    specPage.getFirstNameField().sendKeys(informations[0].FirstName)
    specPage.getLastNameField().sendKeys(informations[0].LastName)
    specPage.getEmailField().sendKeys(informations[0].Email)
    specPage.getAgeField().sendKeys(informations[0].Age)
    specPage.getSalaryField().sendKeys(informations[0].Salary)
    specPage.getDepartmentField().sendKeys(informations[0].Department)
})

Then('User should see wanted informations', async function (dataTable) {
    informations = dataTable.hashes()
    await specPage.getFieldValue(informations[0].FirstName).then(text => {
        expect(text).to.equal(informations[0].FirstName)
    })
    await specPage.getFieldValue(informations[0].LastName).then(text => {
        expect(text).to.equal(informations[0].LastName)
    })
    await specPage.getFieldValue(informations[0].Age).then(text => {
        expect(text).to.equal(informations[0].Age)
    })
    await specPage.getFieldValue(informations[0].Email).then(text => {
        expect(text).to.equal(informations[0].Email)
    })
    await specPage.getFieldValue(informations[0].Salary).then(text => {
        expect(text).to.equal(informations[0].Salary)
    })
    await specPage.getFieldValue(informations[0].Department).then(text => {
        expect(text).to.equal(informations[0].Department)
    })
})

When('User does search by {string}', function (value) {
    specPage.getSearchField().sendKeys(value)
})
