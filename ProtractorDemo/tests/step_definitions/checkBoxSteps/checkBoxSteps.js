let specPage = require('../../pages/checkBox/checkBox.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout, Then, When } = require('cucumber');
const { browser, element } = require('protractor');
const { getResultTextValue } = require('../../pages/checkBox/checkBox.po.js');
var EC = protractor.ExpectedConditions;
setDefaultTimeout(60 * 1200);

Before(function () {
    specPage.init();
})

When('User clicks on Home checkbox', function () {
    specPage.getHomeCheckBox().click()
})

Then('User should see result message {string}', async function (message) {
    var expectedResult = message.split(" ")
    var actualResult = await getResultTextValue()
    var actualWords = actualResult.split("\n")

    expect(actualWords[0]).to.equal('You have selected :')

    for (let index = 1; index < actualWords.length; index++) {
        const element = actualWords[index];
        expect(element).to.equal(expectedResult[index - 1])
    }
})

When('User clicks on Expand all button', function () {
    specPage.getExpandAllButton().click()
})

Then('Chooses checkboxes that he wants {string}', async function (value) {
    await specPage.getCheckBox(value).click()
})